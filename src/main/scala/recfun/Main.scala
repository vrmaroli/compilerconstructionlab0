package recfun

object Main {

  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }

    println()

    println("Parenthesis Balancing")
    val testSet:Array[String] = Array(
        "first test",
        "(second test)",
        "())( third test",
        ")(          ",
        "(()(()())(()))",
        "()()(()())"
        )
    println(testSet(0) + "\t" + true + "\t" + balance(testSet(0).toList))
    println(testSet(1) + "\t" + true + "\t" + balance(testSet(1).toList))
    println(testSet(2) + "\t" + false + "\t" + balance(testSet(2).toList))
    println(testSet(3) + "\t" + false + "\t" + balance(testSet(3).toList))
    println(testSet(4) + "\t" + true + "\t" + balance(testSet(4).toList))
    println(testSet(5) + "\t" + true + "\t" + balance(testSet(5).toList))

    println()
    
    println("Counting Change")
    println("10\t" + "1, 2, 5\t" + countChange(10, List(1, 2, 5)))
    println("5\t" + "1, 2, 5\t" + countChange(5, List(1, 2, 5)))
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if(c==0 || r==c) 1
    else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def innerFunction(chars: List[Char], count: Int): Boolean = {
      if(chars.isEmpty) {
        count == 0
      }
      else if(count < 0) {
        false
      }
      else if(chars.head == '(') {
        innerFunction(chars.tail, count + 1)
      }
      else if(chars.head == ')') {
        innerFunction(chars.tail, count - 1)
      }
      else {
        innerFunction(chars.tail, count)
      }
    }
    innerFunction(chars, 0)
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    if(money<0 || coins.isEmpty) 0
    else if(money==0) 1
    else countChange(money, coins.tail) + countChange(money - coins.head, coins)
  }

}
